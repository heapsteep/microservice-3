package com.heapsteep.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyController {
	
	@GetMapping("/get3")
	public String get4() {
		return "Success from microservie-3";
	}
}
